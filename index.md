---
layout: base.njk
---

{% for character in collections.characters %}
* [{{ character.data.character.name }}]({{character.url}})
{% endfor %}

{% for post in collections.post %}
* [{{ post.data.title }}]({{post.url}})
{% endfor %}