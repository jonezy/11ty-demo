const fetch = require("node-fetch");

module.exports = async () => {
    const request = await fetch("https://rickandmortyapi.com/api/character/");
    const json = await request.json();
    return json.results;
};